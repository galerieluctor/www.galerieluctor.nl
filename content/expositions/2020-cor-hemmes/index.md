---
date: 2020-06-06T19:00:45.554Z
endDate: 2020-07-12T19:00:45.742Z
title: 'Cor Hemmes '
publishDate: '2020-01-01T00:00:00.000Z'
resources:
  - name: featured_image
    src: CorHemmes2.jpg
gallery:
  - image: CorHemmes1.jpg
  - image: CorHemmes2.jpg
  - image: CorHemmes3-e1590325304415.jpg
  - image: AnnaRavenhorst1.jpg
  - image: AnnaRavenhorst2.jpg
  - image: AnnaRavenhorst3.jpg
  - image: BramRavenhorst1.jpg
  - image: BramRavenhorst2.jpg
  - image: BramRavenhorst3.jpg
---
Vanaf 6 juni huisvesten we een ‘familie expositie’: grootvader Cor Hemmes en kleinkinderen Anna Ravenhorst en Bram Ravenhorst.

**Cor Hemmes** (1944) is een autodidacte kunstenaar met als expressiemiddel GLAS. Altijd op zoek naar de werking van licht in glas: kleurrijk of blank. Een nieuwe richting in zijn werk zijn de torso’s, waarvan er twee opgenomen zijn in de expositie: de een massief gegoten glas, de andere opgebouwd uit kleine korrels in de zgn. Pâte de Verre -techniek. Graag combineert hij glas met andere materialen, zoals R.V.S., roestig ijzer of drijfhout enz. Zijn werk omvat kunst voor in de tuin en voor binnenshuis.

**Anna Ravenhorst** (1997) is een zeer getalenteerde kunstenares met een grote voorliefde voor het werken in zwart/wit.Tekeningen in Oostindische inkt, potlood of in linosnede hebben haar voorkeur.Opvallend in haar werk is de verfijndheid en aandacht voor het detail.

**Bram Ravenhorst** (1994) is kunstschilder, graficus en podiumartiest/rapper. In zijn “one-liners” – tekeningen/schilderijen met een doorlopende lijn die de hele figuur omvat – probeert hij de wereld om hem heen te kaderen.

Zo verschillend, maar verbonden door de kunst. De familie-expositie loopt tot 12 Juli.
