---
date: 2023-07-23T08:40:44.546Z
endDate: 2023-08-27T08:40:44.557Z
title: Antoinette van Rij
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: image1.jpg
  - image: image2.jpg
  - image: image3.jpg
---
Vanaf eind juli t/m eind augustus exposeert Antoinette van Rij.
Het tekenen zat Antoinette al van jongs af aan in het bloed. Toen haar kinderen groter werden, pakte ze ook het schilderen op. Ze hobbelde jarenlang van cursus naar les en genoot van het werken in een groep en van alles wat ze er opstak. Ze heeft een enorme drive om te schilderen. “Als ik een paar dagen niet geschilderd heb, dan merk ik dat ik chagrijnig word. Als ik aan het schilderen ben, dan kan ik alles vergeten”.  Op de Kunstacademie Eeklo in België kwam haar passie voor abstracte schilderkunst echt tot bloei. “Abstractie geeft vrijheid, het is een proces en emotie die je wilt overbrengen op het doek.”
Antoinette haalt haar inspiratie uit de natuur, dicht bij huis (ze woont in Burgh-Haamstede). Het strand en de duinen zijn haar favoriete plekken: “Na elke wisseling van het tij, zien de eb- en vloedlijn er anders uit en spoelt er van alles aan uit de zee.” Haar abstracte werk is gebaseerd op alles wat ze vindt tijdens wandelingen langs de kust. Een kreeftenschaar, krabbenrug, blaaswier of schelpen. De vondsten worden vergroot uitgebeeld, abstract maar steeds in de natuurlijke kleuren.

website: https://antoinettevanrij.nl
