---
date: 2022-06-12T07:20:05.846Z
endDate: 2022-07-18T07:20:05.863Z
title: Wilma Frederiks
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: a1.jpg
gallery:
  - image: westerschelde.jpg
  - image: a2.jpg
  - image: cote-d-azur.jpg
---
Wilma Frederiks is afgestudeerd choreografe en yogadocent (sinds 1992). Ze woont en werkt in Amsterdam. Vanaf de jaren ’80 tekent en schildert ze al, aanvankelijk op klein formaat. Meer recent werkt ze vooral met olieverf op wat grotere formaten. Haar fascinatie met de dynamiek en expressie van lichaamstaal, is vergelijkbaar met wat ze beleeft met schilderen.
Wilma gebruikt de foto-camera als een soort schetsboek. Foto’s worden ingezet voor de compositie, maar als het schilderij vordert, worden de foto’s losgelaten en gaat het schilderij een eigen leven leiden. “Ik start met een opzet in houtskool, soms is dit alleen een horizonlijn. De plaatsing van de horizon op het doek is een belangrijk moment, het bepaalt de verdere opbouw van het schilderij. “
De schilderijen kunnen omschreven worden als geabstraheerde landschappen. Wilma werkt altijd met een vooropgezet plan, maar ze gaat wel intuïtief te werk, actie en reactie. Er komen altijd toevalligheden naar voren tijdens het werken, daar wordt gebruik van gemaakt, en dat heeft invloed op het verdere verloop van het schilderij.
“Het schilderen geeft mij een gevoel van vrijheid en is uiteindelijk een vorm van magie.”
Website: https://www.wilmafrederiks.nl