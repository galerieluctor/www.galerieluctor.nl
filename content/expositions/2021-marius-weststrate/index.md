---
date: 2021-07-24T11:36:39.355Z
endDate: 2021-08-29T11:36:39.403Z
title: Marius Weststrate
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: boom1.png
  - name: featured_image
gallery:
  - image: boom1.png
  - image: marius1.jpg
  - image: marius2.jpg
  - image: marius3.jpg
  - image: marius4.jpg
  - image: marius5.jpg
  - image: marius6.jpg
  - image: marius7.jpg
---
Marius Weststrate uit Wemeldinge maakt fijne tekeningen met kroontjespen en fineliner. Sommige tekeningen worden subtiel ingekleurd met kleurpotlood. Zijn onderwerpen zijn landschappen, gebouwen, en bomen. Hij heeft veel aandacht voor detail. Zijn tekeningen zijn doorwerkt en beschrijvend, maar hebben witruimte zodat ze blijven ademen. Zijn liefde voor het Zeeuwse landschap is ook zichtbaar in dorpsgezichten en bruggen, waarmee hij laat zien dat niet alleen het voorspelbaar pittoreske hem trekt. Maar bomen zijn één van zijn favoriete onderwerpen. Hij zegt: bomen getuigen van belangrijke gebeurtenissen. Onze voorouders plantten vaak bomen bij bijzondere gelegenheden, en ze werden gebruikt als grensmarkering of voor religieuze gebruiken. Hij tekent de bomen het liefst in wintertooi, omdat dan de groeivorm, de bast en de knoesten duidelijk zichtbaar zijn. Het op nieuw ontdekken van steeds weer andere bomen, opnieuw de charme gewaarworden, dat is voor hem iedere keer weer een bron van vreugde. De bomen nodigen hem als het ware uit hen te tekenen.