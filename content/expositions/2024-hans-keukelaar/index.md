---
date: 2024-08-31T08:40:44.546Z
endDate: 2024-10-06T08:40:44.557Z
title: Hans Keukelaar, schilderijen
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.png
gallery:
  - image: keukelaar1.jpg
  - image: keukelaar2.jpg
  - image: keukelaar3.jpg
  
 
---
Hans Keukelaar begon met tekenen en schilderen in zijn HBS-tijd onder  leiding van de tekenleraar Hens van der Spoel. Na de schooltijd trok hij weg uit Zeeland en werkte in Weesp, Amersfoort en in Groningen en Friesland. In Amersfoort volgde hij de opleiding aan de Academie voor Beeldende Vorming en in het noorden werd hij hoofddocent voor de beeldende vakken aan de lerarenopleiding Ubbo Emmius.
Al die tijd werkte hij als gouacheschilder met figuratieve en abstracte thema's uit zijn omgeving.
Reeds ruim 20 jaar terug in Zeeland beïnvloedt de natuurlijke omgeving hem extra : landschap,land water en lucht is voor hem het belangrijkst. Door intens bezoek aan musea,en tentoonstellingen blijft hij op de hoogte van de ontwikkelingen in de beeldende kunsten.

