---
date: 2022-04-30T08:16:02.674Z
endDate: 2022-06-05T08:16:02.682Z
title: Expositie Dineke Zwolle
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: vierkant.jpg
---
Dineke houdt van schetsen, tekenen en schilderen, en ze werk het liefst met zoveel mogelijk verschillende materialen. Van rietpen tot kwast, en van inkt tot olieverf.

Na de academie in België (modelklas) heb ik nu een atelier in Wissenkerke, Voorstraat 1 (Zeeland).

Naast de oerelementen vuur, aarde, lucht en water ben ik gefascineerd door de magie van uilen,  en ... ‘omdat een uil ook moet eten’, ook door muizen!

Website: www.dina-inez.com