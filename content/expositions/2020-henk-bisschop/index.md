---
date: 2020-07-18T10:55:26.825Z
endDate: 2020-08-23T10:55:27.058Z
title: Henk Bisschop
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: a.jpg
gallery:
  - image: a.jpg
  - image: b.jpg
  - image: c.jpg
  - image: d.jpg
  - image: e.jpg
  - image: f.jpg
  - image: g.jpg
---
Henk Bisschop (Wemeldinge) is onze exposant vanaf 18 juli.  Henk aquarelleert reeds 25 jaar, is autodidact. Het is voor hem elke keer weer een uitdaging om met verf en water en met gebruik van verschillende technieken iets moois op papier te zetten.  Hij heeft al vaak geëxposeerd, zowel regionaal als landelijk, en is heel blij zijn werk nu in de mooie ruimte van Galerie Luctor  te kunnen laten zien.  In zijn woonplaats Wemeldinge is hij oprichter van de Kunst & Natuurroute en ieder jaar vaste deelnemer.
Oudelande heeft een speciale plek in zijn leven, omdat zijn opa (van moeders kant)  Matheus v.d. Velde rond 1900 in dit dorp woonde en werkte. Matheus was huisschilder, (bekend om zijn "marmer schilderen"), fotograaf, organist, winkelier en in zijn vrije tijd kunstschilder met olieverf. Hij schilderde zeer diverse onderwerpen en Henk is trots een schilderij van zijn opa tussen zijn eigen aquarellen te exposeren en beleeft dit als een samenkomen van generaties op kunstzinnig gebied.

Het wordt zeer op prijs gesteld als u Henk's werk en dat van zijn opa komt bekijken.

Als extra bij deze expositie heeft de aquarellist 100 kleine schilderijtjes gemaakt (9 x 20 cm), die  hij verkoopt voor de prijs van 3 euro. De gehele opbrengst hiervan gaat naar “Voedselbank De Bevelanden”.

Contact: Henk Bisschop, Dorpsstraat 82 4424 CZ Wemeldinge, tel 0113622543 of 0653748037, <a href="mailto:hbisschop@zeelanddummynet.nl" title=Email class="has-text-white">hbisschop@zeeland<span id="dummy">REMOVE</span>net.nl</a>

Website: [henkbisschop.nl](http://henkbisschop.nl/)
