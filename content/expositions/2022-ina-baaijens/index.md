---
date: 2022-07-23T07:21:14.878Z
endDate: 2022-08-29T07:21:14.888Z
title: Ina Baaijens
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: zuid-beveland-fruitbomen.jpg
gallery:
  - image: domburg-strand.jpg
  - image: oostkerk-middelburg.jpg
  - image: boerderij-op-walcheren.jpg
  - image: zuid-beveland-fruitbomen.jpg
  - image: zeeuws-meisje.jpg
---
Ina Baijens woont en werkt in Amsterdam en bepaalde delen van het jaar ook in Frankrijk. Ze heeft haar wortels in Zeeland en gaat daar graag met enige regelmaat naar terug.
Na het beëindigen van haar werkzame leven als jurist/socioloog in 2006 kwam er veel tijd vrij om te tekenen en te schilderen, iets waar ze voordien maar zo nu en dan tijd voor had.
Ina werkt vooral met olieverf. Favoriete onderwerpen zijn landschappen, steden en ook het schilderen van portretten. “Ik schilder kleurrijk en realistisch. Het liefst werk ik “plein air”, want dan zie je zoveel meer nuances in je omgeving dan je op een foto kan vastleggen. Het blijft iedere keer weer spannend om datgene wat je waarneemt, vorm te geven op het doek”.
Op deze tentoonstelling zijn veel Zeeuwse landschappen en steden te zien, als een hommage aan Zeeland. Ook te zien een tweetal portretten van Zeeuwse meisjes. “Zij dragen de klederdracht die mijn moeder ook droeg toen ze jong was en die zij als één van de laatsten op Walcheren haar hele leven heeft gedragen”.
Voor meer informatie: www.inabaaijens.nl