---
date: 2024-06-08T08:40:44.546Z
endDate: 2024-07-14T08:40:44.557Z
title: Amber van Twist
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: amber0.jpg
  - image: amber1.jpg
  - image: amber2.jpg
  - image: Pluto.jpg
  
 
---
Amber van Twist (Oudelande, 2001) heeft tekenen en schilderen altijd al leuk gevonden. Toen de gelegenheid daar was heeft ze de stoute schoenen aangetrokken en zich ingeschreven bij de kunstacademie. Daar bleek dat ze er best wel goed in was. In de vier jaar kunstacademie (nog één jaar te gaan) heeft ze veel geleerd over schilderen, maar vooral ook gewerkt aan het ontwikkelen van een eigen stijl.
 
Het resultaat van die ontwikkeling is te zien in de expositie in Galerie Luctor. Haar schilderijen worden gekenmerkt door intense en vrolijke kleuren. Zelf noemt ze haar stijl kinderlijk, een resultaat van kinderlijke expressie. Anders gezegd, past haar schilderstijl in de betrekkelijk jonge traditie van de naïeve kunst. Dat is zeker geen denigrerende term, het is een genre dat zijn plaats veroverd heeft in de kunstwereld. En ja, er komt nogal eens een teddybeer voor in Ambers recente schilderijen. De schilderijen zijn veelal gecomponeerd vanuit een centraal punt dat gedetailleerd is uitgewerkt; naar buiten toe wordt het dan iets vager. Ze neemt de vrijheid om onbeschaamd wat kleur toe te voegen aan de grijze massa van de wereld.


