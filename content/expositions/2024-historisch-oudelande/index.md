---
date: 2024-03-23T08:40:44.546Z
endDate: 2024-04-26T08:40:44.557Z
title: Historisch Oudelande
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: IMG67.jpg
  - image: Oudelande2.jpg
  - image: Stelletje.jpg  
  
 
---
In de maand april richten we ons op het dorp Oudelande van 100 jaar geleden. Er zijn fotografische vergrotingen van oude prentbriefkaarten, voorzien van commentaar. De foto’s geven een mooi beeld van hoe het er een eeuw geleden aan toe ging in Oudelande. Ook zijn er foto’s van (destijds) jonge stelletjes, uiteraard in klederdracht. Onze trouwe winkeljuffrouw is voor de gelegenheid in traditionele Zuid-Bevelandse dracht gestoken. Haar verloofde is er even tussenuit geknepen, maar hij heeft z’n jas achter gelaten. De fraai versierde knopen van de boerenjas krijgen speciale aandacht. Het pand waarin nu Galerie Luctor is gevestigd, was in het verleden een bakkerij. Een gerestaureerd winkelbord herinnert daaraan. Tevens zijn er wat bakkerijspullen neergelegd. Een belangrijk beroep in die tijd was dat van wagenmaker. Allerlei originele wagenmakersspullen worden tentoon gesteld. 
Om het spannend te maken is er ook nog een historisch puzzeltje: men kan zijn/haar kennis van het Oudelandse verleden testen. Voor goede oplossingen is een klein prijsje beschikbaar. 

