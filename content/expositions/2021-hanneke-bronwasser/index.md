---
date: 2021-06-12T15:41:15.171Z
endDate: 2021-07-18T15:41:15.219Z
title: Hanneke Bronwasser
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: boom.png
gallery:
  - image: boom.png
  - image: img_20180502_180515.jpg
  - image: zonnebloemen-3.jpg
  - image: zomeranemonen.jpg
  - image: img_20180502_180244.jpg
  - image: noordpool.jpg
  - image: img_20210516_131237.jpg
---
De expositie “Buiten-gewoon” omvat aquarellen van Hanneke Bronwasser. Alle schilderijen hebben iets met de natuur te maken. Sommige werken zijn tamelijk figuratief, andere zijn bijna abstract zodat je even goed moet kijken wat het voorstelt.
Hanneke is geboren en getogen in Sas van Gent, maar woont sinds haar pensionering het grootste deel van het jaar in Frankrijk, in de Lot- en Garonne streek. In de jaren 70 volgde ze opleidingen op de kunstacademie van Eeklo in olieverf-, aquarel- en etstechnieken. Sinds 2000 werkt ze voornamelijk met aquarelverf. Ze werkt soms op locatie, maar ook worden (zelf gemaakte) foto’s gebruikt.  Bij het bepalen van de kleuren van haar werk gaat ze in eerste instantie uit van drie of vier kleuren. In een voorstudie worden kleine kleurmengingen gemaakt en daar wordt vrijwel niet meer van afgeweken. Dit beperkte palet maakt dat haar schilderijen een evenwichtig geheel worden en prettig zijn om naar te kijken.

Bijzonder is het materiaal waarop ze werkt: *“In 2016 ontmoette ik een collega schilder die op Yupo werkte. Dat materiaal intrigeerde me vanaf het eerste moment. Men noemt het papier maar het is in feite synthetisch materiaal dat voor het grootste deel bestaat uit polypropyleen.*” Yupo is ontwikkeld in Japan en oorspronkelijk bedoeld voor drukwerktechnieken, bijvoorbeeld voor posters en voor glossy brochures. Het bestaat in verschillende kwaliteiten. Yupo is superglad en de verf droogt niet in, maar op het papier. Dat geeft een bijzonder effect en dat is meteen ook de uitdaging, het is niet gemakkelijk om op te werken en de droogtijd is lang. “*Het mag duidelijk zijn dat ik hierbij niet op locatie werk, maar in mijn atelier. Eén verkeerde beweging en de verf loopt een ongewenste richting uit*.”