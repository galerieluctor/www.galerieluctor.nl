---
date: 2023-06-10T08:40:44.546Z
endDate: 2023-07-16T08:40:44.557Z
title: Floor Kurstjens
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: geisha_exto.jpg
  - image: 20230130_121644-2-.jpg
  - image: hunab-kuh-e.jpg
  - image: zonsondergang-exto.jpg
  - image: toscane_2-exto.jpg
---
In de periode juni/juli exposeert Floor Kurstjens in de galerie. Haar schilderijen hebben veelal de realiteit als uitgangspunt, maar neigen naar abstractie. Wat erg opvalt is de enorme kleurigheid van haar werken.
Floor is afkomstig uit Tegelen (Limburg) en gebruikt diverse technieken: collage, acryl, aquarel, pastel en olieverf. Ze heeft vele jaren lessen gevolgd in tekenen en schilderen bij gerenommeerde kunstenaars en bij het Kunstencentrum in Venlo.
Floor werkt intuïtief, spelend op de grenzen tussen vorm en chaos, en tussen fantasie en werkelijkheid. Hoe de wereld eruitziet kan iedereen zelf waarnemen, maar de verborgen kracht erachter probeert ze door middel van kleur voelbaar te maken. Inspiratie haalt ze uit de natuur, dieren en symboliek van oude culturen. Het is een zoeken naar de oerkiem van alles, naar de bron van verbinding.

website: https://florentine.exto.nl
