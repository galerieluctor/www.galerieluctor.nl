---
date: 2024-07-20T08:40:44.546Z
endDate: 2024-08-25T08:40:44.557Z
title: Franklin Gelderland, fotografie
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: 1_Website.jpg
  - image: 2_Website.jpg
  - image: 3_Website.jpg
  
 
---
Franklin Gelderland is altijd op zoek naar nieuwe uitdagingen binnen zijn favoriete foto-stijl: mensen fotograferen. Hij werd geïnspireerd door beelden van Rodin, alsook door een aantal kleurrijke schilderijen. Hij kreeg het idee om geportretteerden in beeld te gaan brengen, naar een beeld zoals Rodin, maar ook andere beeldhouwers. Naar analogie van beelden uit de geschiedenis, waar een hoofd of ledemaat ontbreekt, heeft hij een nieuwe visie ontwikkeld, waarbij deze elementen, waar mogelijk, worden weggelaten.
Na veel experimenteren is Franklin protretfoto’s gaan combineren met creatieve materialen en natuurlijke elementen en het toevoegen van de verf, soms voorafgaand aan het maken van de foto, soms achteraf middels beeldbewerking. Bij sommige elementen zie je dieptewerking, bij andere straalt deze rust uit. In zijn werken is het onherkenbare inmiddels de rode draad geworden.
Binnen deze expositie kunt u de verschillende creaties en richtingen zien, waaraan hij de laatste jaren aan heeft gewerkt heb, en die in de toekomst verder uitgediept zullen worden.

Email: foto@fotofranky.nl Website: https://franklin-art-photography.nl/

