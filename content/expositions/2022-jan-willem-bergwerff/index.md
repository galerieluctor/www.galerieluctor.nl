---
date: 2022-09-03T14:07:04.844Z
endDate: 2022-10-10T14:07:04.855Z
title: Jan Willem Bergwerff
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: img_5546.jpg
gallery:
  - image: img_5554.jpg
  - image: img_5553.jpg
  - image: img_5545.jpg
  - image: img_5536.jpg
---
Jan Willem Bergwerff woont tijdens de wintermaanden in Vlissingen, de overige maanden is hij woonachtig in Oudelande. Vanaf zijn jonge jaren heeft hij een voorliefde voor kunst en werd hij aangetrokken tot het realistische werk.


Jan Willem is autodidact en heeft zich de realistische kunst eigen gemaakt, voornamelijk met olieverf op doek. Met de bekende kunstschilder Aad Hofman heeft hij samengewerkt in diens atelier.


De laatste jaren is hij zijn talenten gaan verbreden op het gebied van mixed media. Dit levert vaak zeer verrassend werk op de de stijl van het magisch realisme. Hierbij worden verschillende materialen verwerkt, zoals inkt, aquarelverf, goud, zilver en/of parelmoer. Hij doet dit op een geheel eigen wijze, resulterend in een aparte kunstvorm. Hij maakt ook zeer verfijnde potloodtekeningen, die een surrealistisch beeld geven van de onzichtbare werkelijkheid. “Ik kijk om me heen en neem waar, en datgene wat snel verdwijnt maak ik zichtbaar in mijn werk.” 


Van dit alles is een uitgebreide selektie te zien en te bewonderen op de expositie in Galerie Luctor.