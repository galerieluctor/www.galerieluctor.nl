---
date: 2024-04-28T08:40:44.546Z
endDate: 2024-06-02T08:40:44.557Z
title: Loes Müller, Agnes den Hartogh
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: Loes.jpg
  - image: Loes1.jpg
  - image: Loes2.jpg
  - image: Hartogh.jpg
  - image: Hartogh1.jpg
  - image: Hartogh2.jpg
  - image: Hartogh3.jpg
  
 
---
In de maand mei hebben we een dubbel-expositie: Loes Müller en Agnes den Hartogh

Loes Müller begon met tekenen toen zij ongeveer 7 à 8 jaar oud was, hierbij aangespoord door haar vader die heel veel pentekeningen maakte. Zij is nog kort op tekenles geweest bij Frans Naerebout, een Vlissingse schilder. Na jaren niets kunstzinnigs gedaan te hebben (ander werk en de kinderen), is zij rond 1994 aquarel-lessen gaan nemen bij Jos Bogaard en tegelijkertijd volgde zij een half jaar beeldhouwen bij het MIKV te Middelburg. 
Met het beeldhouwen is zij doorgegaan. Zij werkt hoofdzakelijk in mergel, maar ook in albast en speksteen. Ze is zich ook gaan toeleggen op beelden van brons. Loes laat zich altijd weer inspireren door nieuwe dingen. Meer recent uit zich dat in het maken van sieraden in allerlei vormen en met allerlei materialen.

Agnes den Hartogh is professioneel grafisch ontwerper en illustrator. Ze heeft les gegeven aan de Hogeschool Grafisch Ontwerp in Amsterdam en gewerk voor bedrijven als KLM, Rabobank, Tommy Hilfiger. Ze reist de wereld rond met haar schetsboek, waarin ze haar indrukken vast legt met snel gemaakte tekeningen. In haar werk zijn sporen terug te vinden van bezoeken aan Italië, Frankrijk, Polen, Cuba, Argentië, Kenia, en nog vele andere landen. De snelle schetsen vinden deels een uitwerking in schilderijen, collage en drukwerk. Agnes heeft een basis in Vlissingen, maar ook een atelier in Italië, waar ze workshops geeft op het gebied van experimentele druktechnieken. Tentoonstellingen waren er in Nederland, Italië en andere landen.

