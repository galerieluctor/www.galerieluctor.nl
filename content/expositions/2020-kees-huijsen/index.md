---
date: 2020-08-29T17:53:11.748Z
endDate: 2020-10-04T17:53:11.804Z
title: Kees Huijsen
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: 38-zak-van-zuid-beveland.jpeg
gallery:
  - image: 38-zak-van-zuid-beveland.jpeg
  - image: 46-bij-nisse.jpeg
  - image: 82-zak-v-zuid-beveland.jpeg
  - image: 39-kattendijke.jpeg
  - image: 95-zonsondergang.jpeg
  - image: 52-winter.jpeg
  - image: 96-yersekse-moer.jpeg
---
## Kleurrijke schilderijen

Dynamiek is belangrijker dan gelijkenis in een schilderij. Ik probeer beweging te suggereren door bijvoorbeeld “dode” huizen als beweeglijke objecten te zien.  De huizen moeten naar elkaar kijken, praten ………….elkaar versterken. Door de kleuren explicieter te kiezen, de vormen te overdrijven en lijnen aan het beeld toe te voegen probeer ik “dode” onderwerpen dynamischer te maken. Ik gebruik de werkelijkheid om een schilderij te maken. Het gaat mij om de interpretatie. Het is de werkelijkheid op een andere wijze uitgebeeld.

Vanaf de jaren ’70 schilder ik met aquarel, olie- en acrylverf. Momenteel schilder ik veel met acrylverf op doek. De kleuren van acrylverf zijn feller zijn dan van olieverf. 
De acrylverf breng ik onverdund op het doek aan. Dit betekent dat de verf direct uit de tube wordt gebruik. Door de korte droogtijd kan ik snel en eenvoudig meerdere lagen over elkaar aanbrengen. Het resultaat geeft mooie kleuren met scherpe randen.
Door de technische mogelijkheden van acrylverf kan ik mij uitdrukken wat ik voel. Geliefde onderwerpen zijn stadsgezichten of onderwerpen die met water te maken hebben, zoals havens, boten en scheepswerven.

Ik schilder een onderwerp, omdat het mij aanspreekt en niet omdat het onderwerp goed in een serie past.  Ik wil op geen enkele wijze mijzelf beperkingen opleggen in onderwerp keuze, kleur, afmeting of wat dan ook. 

Mijn werk is regelmatig te zien in verschillende galeries. Er zijn o.a. tentoonstellingen geweest  in Rotterdam, Doetinchem, Biervliet, Terneuzen, Zelzate (B), Philippine, Middelburg, Vlissingen, Goes en Kapelle. 

Kees Huijsen  (CBK-lid, docent schilderen). Kijk voor meer informatie www.keeshuijsen.nl