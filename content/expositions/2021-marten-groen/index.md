---
date: 2021-09-04T07:46:01.002Z
endDate: 2021-10-10T07:46:01.058Z
title: Marten Groen
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: edelhert.jpg
gallery:
  - image: keltische-knoop.jpg
  - image: golven.jpg
  - image: p1010012.png
  - image: img-20200723-wa0007.jpg
---
Marten Groen is een veelzijdig kunstenaar uit het Brabantse Boxtel. Zijn bekendheid verwierf hij vooral met monumentale beelden in Cortenstaal. Het grootste werk van zijn hand is onlangs geplaatst bij het station in Boxtel: een beeld van vier meter hoog, genaamd Edelhert. In de  expositie in galerie Luctor is een schaalmodel (1:16) van dit beeld te zien en te koop.
Verder zijn er schilderijen (in zwart-wit)  en beeldhouwwerken te zien.
Er is een serie schilderijen getiteld “Beiroet” die is geïnspireerd op de explosie aldaar van 4 augustus 2020. Een verwoestende kracht raasde over de stad en zijn bewoners, alles zwart achterlatend. Het thema “Golven” zien we in zowel beeldhouwwerken als schilderijen. Golven van de zee (Zeeuwse golven zegt Marten zelf), of golvend riet. 
Er is een herdenkingszuil of stiltezuil aanwezig, met een uitgehakte tekst. Als gevolg van de Corona epidemie ging er een stilte door het land. De tekst op de zuil is “‘De gehoorde stilte” en op de andere zijde “De stilte gehoord”. Andere beelhouwwerken zijn o.a. Keltische knopen uitgehakt in wit natuursteen: “Bianco del Mare”.